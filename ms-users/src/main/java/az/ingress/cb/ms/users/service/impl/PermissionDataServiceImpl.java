package az.ingress.cb.ms.users.service.impl;

import az.ingress.cb.ms.users.dao.UserCustomerEntity;
import az.ingress.cb.ms.users.dao.repository.UserCustomerRepository;
import az.ingress.cb.ms.users.model.enums.PaymentPermissionType;
import az.ingress.cb.ms.users.model.view.permission.AccountDataView;
import az.ingress.cb.ms.users.model.view.permission.CustomerDataView;
import az.ingress.cb.ms.users.model.view.permission.CustomerUserView;
import az.ingress.cb.ms.users.model.view.permission.UserDataView;
import az.ingress.cb.ms.users.service.PermissionDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class PermissionDataServiceImpl implements PermissionDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PermissionDataService.class);

    private final UserCustomerRepository userCustomerRepository;

    public PermissionDataServiceImpl(UserCustomerRepository userCustomerRepository) {
        this.userCustomerRepository = userCustomerRepository;
    }

    @Override
    public List<CustomerUserView> getAllCustomersAndUsers() {
        var userCustomerEntities = userCustomerRepository
                .findAllCustomersAndUsers();

        LOGGER.debug("userCustomerEntities count = {}", userCustomerEntities.size());

        var customerMap =
                userCustomerEntities
                        .stream()
                        .filter(Objects::nonNull)
                        .collect(Collectors.groupingBy(UserCustomerEntity::getCustomerId));

        var customerUserViews =
                customerMap
                        .entrySet()
                        .stream()
                        .map(entry -> CustomerUserView
                                .builder()
                                .customerId(entry.getKey())
                                .users(entry
                                        .getValue()
                                        .stream()
                                        .filter(Objects::nonNull)
                                        .map(UserCustomerEntity::getUserId)
                                        .collect(Collectors.toList()))
                                .build())
                        .collect(Collectors.toList());

        return customerUserViews;
    }

    @Override
    public List<CustomerDataView> getCustomerPermissionData() {
        var accountDataView1 = AccountDataView.builder()
                .accountNo("33803019449783589206")
                .notPermittedViewTypes(Arrays.asList("SALARY_PAYMENT", "INTERNATIONAL_PAYMENT"))
                .notPermittedCreateTypes(Arrays.asList("SALARY_PAYMENT", "INTERNATIONAL_PAYMENT", "OWN_ACCOUNTS_PAYMENT"))
                .build();
        var accountDataView2 = AccountDataView.builder()
                .accountNo("98561019449783589206")
                .notPermittedViewTypes(Collections.emptyList())
                .notPermittedCreateTypes(Collections.emptyList())
                .build();

        var userDataView = UserDataView.builder()
                .id(1L)
                .accounts(Arrays.asList(accountDataView1, accountDataView2))
                .build();

        var customerDataView = CustomerDataView.builder()
                .id("9783589")
                .users(Arrays.asList(userDataView))
                .build();


        return Arrays.asList(customerDataView);
    }

    @Override
    public List<String> getPaymentTypes() {
        List<PaymentPermissionType> paymentTypeList = Arrays.asList(PaymentPermissionType.values());

        List<String> paymentTypes =
                paymentTypeList
                        .stream()
                        .map(Enum::name).collect(Collectors.toList());

        return paymentTypes;
    }

}
