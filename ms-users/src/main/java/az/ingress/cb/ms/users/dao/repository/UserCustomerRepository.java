package az.ingress.cb.ms.users.dao.repository;

import az.ingress.cb.ms.users.dao.UserCustomerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface UserCustomerRepository extends CrudRepository<UserCustomerEntity, Long> {

    @Query("select new az.ingress.cb.ms.users.dao.UserCustomerEntity(c.userId, c.customerId, c.isSuperUser) " +
            "from UserCustomerEntity c")
    List<UserCustomerEntity> findAllCustomersAndUsers();

}
