package az.ingress.cb.ms.users.dao;

import az.ingress.cb.ms.users.model.enums.PaymentPermissionType;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_account_permission")
public class UserAccountPermissionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_account_id")
    private Long userAccountId;

    @Column(name = "payment_type")
    @Enumerated(EnumType.STRING)
    private PaymentPermissionType type;

    @NotNull
    @Column(name = "can_view")
    private Boolean view;

    @NotNull
    @Column(name = "can_create")
    private Boolean create;

}
