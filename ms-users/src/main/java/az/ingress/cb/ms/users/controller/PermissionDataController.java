package az.ingress.cb.ms.users.controller;

import az.ingress.cb.ms.users.model.view.permission.CustomerDataView;
import az.ingress.cb.ms.users.model.view.permission.CustomerUserView;
import az.ingress.cb.ms.users.service.PermissionDataService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("${ms.internal.url}")
public class PermissionDataController {

    private final PermissionDataService permissionDataService;

    public PermissionDataController(PermissionDataService permissionDataService) {
        this.permissionDataService = permissionDataService;
    }

    @GetMapping("/customers-users")
    public List<CustomerUserView> getAllCustomersAndUsers() {
        return permissionDataService.getAllCustomersAndUsers();
    }

    @GetMapping("/permission-data")
    public List<CustomerDataView> getCustomerPermissionData() {
        return permissionDataService.getCustomerPermissionData();
    }

    @GetMapping("/payment-types")
    public List<String> getPaymentTypes() {
        return permissionDataService.getPaymentTypes();
    }

}
