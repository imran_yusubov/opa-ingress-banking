package az.ingress.cb.ms.users.model.view.permission;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("User data view")
public class UserDataView {

    @ApiModelProperty("User id")
    private Long id;

    @ApiModelProperty("Customer accounts")
    private List<AccountDataView> accounts;

}
