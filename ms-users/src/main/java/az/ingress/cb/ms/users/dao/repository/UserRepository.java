package az.ingress.cb.ms.users.dao.repository;

import az.ingress.cb.ms.users.dao.UserEntity;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @Query(value = "select jsonb_agg(customer_data) as customers\n" +
            "from (select customer_id,\n" +
            "             user_id,\n" +
            "             (select jsonb_agg(account_data) as accounts\n" +
            "              from (select a.account_no,\n" +
            "                           (select jsonb_agg(permissions_data) as payment_permissions\n" +
            "                            from (select p.payment_type, p.can_create, p.can_view\n" +
            "                                  from user_account_permission p\n" +
            "                                  where p.user_account_id = a.id) permissions_data)\n" +
            "                    from user_account a\n" +
            "                    where a.user_customer_id = c.id) account_data)\n" +
            "      from user_customer c\n" +
            "      where c.user_id\n" +
            "                in (select id from user)) as customer_data;", nativeQuery = true)
    Optional<ArrayNode> getUserPermissionDataAsJsonArray();
}
