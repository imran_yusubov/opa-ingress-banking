package az.ingress.cb.ms.users.model.view.permission;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Payment permissions data view")
public class PaymentPermissionDataView {

    @ApiModelProperty("Payment type")
    @JsonProperty("payment_type")
    private String paymentType;

    @ApiModelProperty("Create permission")
    @JsonProperty("can_create")
    private Boolean canCreate;

    @ApiModelProperty("View permission")
    @JsonProperty("can_view")
    private Boolean canView;

}
