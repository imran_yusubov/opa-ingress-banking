package az.ingress.cb.ms.users.service;

import az.ingress.cb.ms.users.model.view.permission.CustomerDataView;
import az.ingress.cb.ms.users.model.view.permission.CustomerUserView;
import java.util.List;

public interface PermissionDataService {
    List<CustomerUserView> getAllCustomersAndUsers();

    List<CustomerDataView> getCustomerPermissionData();

    List<String> getPaymentTypes();
}
