package az.ingress.cb.ms.users.model.view.permission;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Account data view")
public class AccountDataView {

    @ApiModelProperty("Account no")
    @JsonProperty("account_no")
    private String accountNo;

    @ApiModelProperty("Not permitted payment types to create")
    @JsonProperty("not_permitted_create_types")
    private List<String> notPermittedCreateTypes;

    @ApiModelProperty("Not permitted payment types to view")
    @JsonProperty("not_permitted_view_types")
    private List<String> notPermittedViewTypes;

}
