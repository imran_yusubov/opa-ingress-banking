package az.ingress.cb.ms.users.model.view.permission;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Customer data view")
public class CustomerDataView {

    @ApiModelProperty("Customer id")
    private String id;

    @ApiModelProperty("List of users")
    private List<UserDataView> users;

}
