package az.ingress.cb.ms.users.model.enums;

public enum UserStatus {
    CREATED,
    CUSTOMER_MAPPED,
    APPROVED,
    VERIFICATION,
    OTP_VERIFICATION,
    ACTIVE,
    INACTIVE,
    BLOCKED,
    DELETED
}
