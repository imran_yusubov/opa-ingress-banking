package az.ingress.cb.ms.users.model.enums;

public enum PaymentPermissionType {
    DOMESTIC_PAYMENT,
    INTERNAL_PAYMENT,
    FX_PAYMENT,
    INTERNATIONAL_PAYMENT,
    OWN_ACCOUNTS_PAYMENT,
    TAX_PAYMENT,
    SALARY_PAYMENT
}
