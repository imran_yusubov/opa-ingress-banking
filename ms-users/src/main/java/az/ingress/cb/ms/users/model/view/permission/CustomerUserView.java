package az.ingress.cb.ms.users.model.view.permission;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Users of customer")
public class CustomerUserView {

    @ApiModelProperty("Customer id")
    @JsonProperty("customer_id")
    private String customerId;

    @ApiModelProperty("List of users of the customer")
    private List<Long> users;

}
