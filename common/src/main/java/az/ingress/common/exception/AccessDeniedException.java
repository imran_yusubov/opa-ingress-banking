package az.ingress.common.exception;

public class AccessDeniedException extends RuntimeException {
    private static final long serialVersionUID = 58432132465811L;

    public AccessDeniedException(String message) {
        super(message);
    }
}
