package az.ingress.common.exception;

public class PaymentAccessDeniedException extends AccessDeniedException {

    private static final long serialVersionUID = 58432132465811L;

    public PaymentAccessDeniedException(String message) {
        super(message);
    }

}
