package az.ingress.cb.own.payment.model.permission;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpaResultResponse {
    private Boolean result;
}
