package az.ingress.cb.own.payment.model;

public final class HeaderKeys {
    public static final String HEADER_USER_ID = "User-ID";
    public static final String HEADER_CUSTOMER_ID = "Customer-ID";

    private HeaderKeys() {
    }
}
