package az.ingress.cb.own.payment.model.dto;

import az.ingress.cb.own.payment.model.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentDto {
    private Long id;
    private String type;
    private Long userId;
    private String customerId;
    private String debitAccount;
    private String payeeName;
    private String creditAccount;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String description;
    private BigDecimal transferAmount;
    private String debitCurrencyCode;
    private String creditCurrencyCode;
    private PaymentStatus status;
    private String rejectReason;
}
