package az.ingress.cb.own.payment.model.enums;

public enum PaymentStatus {
    CREATED,
    DELETED,
    AUTHORIZED,
    FCUBS_SUCCESS,
    FCUBS_ERROR,
    FCUBS_UNEXPECTED_EXCEPTION,
    FCUBS_HOLD,
    FCUBS_ACTIVE,
    FCUBS_REVERSED
}
