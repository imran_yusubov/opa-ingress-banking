package az.ingress.cb.own.payment.service;

import az.ingress.cb.own.payment.model.view.OwnPaymentView;

public interface OwnPaymentService {
    OwnPaymentView createPayment(OwnPaymentView paymentView, String customerId, Long userId);

    OwnPaymentView getPaymentById(Long id, String customerId, Long userId);
}
