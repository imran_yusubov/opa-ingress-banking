package az.ingress.cb.own.payment.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Amount with currency")
public class Amount {
    @ApiModelProperty("Code of currency - RUB, AZN, USD, EUR etc")
    private String currencyCode;

    @ApiModelProperty("Amount value in format 100.01")
    private String value;
}
