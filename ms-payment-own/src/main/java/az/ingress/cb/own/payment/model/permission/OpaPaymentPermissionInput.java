package az.ingress.cb.own.payment.model.permission;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OpaPaymentPermissionInput {

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("account_no")
    private String accountNo;

    @JsonProperty("payment_type")
    private String paymentType;

    @JsonProperty("payment_id")
    private String paymentId;

}
