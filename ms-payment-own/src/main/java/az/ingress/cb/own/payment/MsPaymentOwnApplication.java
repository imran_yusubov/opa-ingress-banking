package az.ingress.cb.own.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsPaymentOwnApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsPaymentOwnApplication.class, args);
    }

}
