package az.ingress.cb.own.payment.model.permission;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OpaPermissionRequest<T> {
    private String policy;
    private T input;
}
