package az.ingress.cb.own.payment.config;

import az.ingress.cb.own.payment.client.OpaPermissionClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(clients = {
        OpaPermissionClient.class
})
public class FeignDefaultConfig {

}
