package az.ingress.cb.own.payment.service.impl;

import az.ingress.cb.own.payment.dao.OwnPaymentEntity;
import az.ingress.cb.own.payment.dao.OwnPaymentRepository;
import az.ingress.cb.own.payment.model.view.OwnPaymentView;
import az.ingress.cb.own.payment.service.OwnPaymentService;
import az.ingress.cb.own.payment.util.OpaPermissionHelper;
import az.ingress.common.exception.PaymentAccessDeniedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class OwnPaymentServiceImpl implements OwnPaymentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OwnPaymentServiceImpl.class);

    private final OwnPaymentRepository ownPaymentRepository;
    private final OpaPermissionHelper opaPermissionHelper;

    public OwnPaymentServiceImpl(OwnPaymentRepository ownPaymentRepository,
                                 OpaPermissionHelper opaPermissionHelper) {
        this.ownPaymentRepository = ownPaymentRepository;
        this.opaPermissionHelper = opaPermissionHelper;
    }

    @Override
    @Transactional
    public OwnPaymentView createPayment(OwnPaymentView paymentView, String customerId, Long userId) {

        var canCreate = !opaPermissionHelper.hasCreatePermission(userId, customerId, paymentView.getDebitAccount());
        if (canCreate) {
            LOGGER.error("access denied to create OWN_ACCOUNTS_PAYMENT userId {}, customerId {}, accountNo {}",
                    userId, customerId, paymentView.getDebitAccount());
            throw new PaymentAccessDeniedException(String.format("access denied to create OWN_ACCOUNTS_PAYMENT"));
        }

        LOGGER.debug("========SUCCESS=========");
        return new OwnPaymentView();
    }

    @Override
    public OwnPaymentView getPaymentById(Long id, String customerId, Long userId) {

        OwnPaymentEntity ownPaymentEntity = ownPaymentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("payment %d not found", id)));

        var canView = !opaPermissionHelper
                .hasViewPermission(userId, customerId, ownPaymentEntity.getDebitAccount());
        if (canView) {
            throw new PaymentAccessDeniedException(
                    String.format("access is denied for userId %s customerId %s to view" +
                            "paymentId {}", userId, customerId, ownPaymentEntity.getId()));
        }
        LOGGER.debug("========SUCCESS=========");
        return new OwnPaymentView();
    }

}
