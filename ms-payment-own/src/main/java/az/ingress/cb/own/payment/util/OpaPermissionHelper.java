package az.ingress.cb.own.payment.util;

import az.ingress.cb.own.payment.client.OpaPermissionClient;
import az.ingress.cb.own.payment.model.permission.OpaPaymentPermissionInput;
import az.ingress.cb.own.payment.model.permission.OpaPermissionRequest;
import az.ingress.cb.own.payment.model.permission.OpaResultResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class OpaPermissionHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(OpaPermissionClient.class);

    private static final String OWN_ACCOUNTS_PAYMENT = "OWN_ACCOUNTS_PAYMENT";
    private final OpaPermissionClient opaPermissionClient;

    public OpaPermissionHelper(OpaPermissionClient opaPermissionClient) {
        this.opaPermissionClient = opaPermissionClient;
    }

    public boolean hasViewPermission(Long userId, String customerId, String accountNo) {
        var input = OpaPaymentPermissionInput.builder()
                .userId(userId)
                .customerId(customerId)
                .accountNo(accountNo)
                .paymentType(OWN_ACCOUNTS_PAYMENT)
                .build();
        return check("user/payment/view", input).getResult();
    }

    public boolean hasCreatePermission(Long userId, String customerId, String accountNo) {
        var input = OpaPaymentPermissionInput.builder()
                .userId(userId)
                .customerId(customerId)
                .accountNo(accountNo)
                .paymentType(OWN_ACCOUNTS_PAYMENT)
                .build();
        return check("user/payment/create", input).getResult();
    }

    private OpaResultResponse check(String policy, OpaPaymentPermissionInput input) {
        final var userId = input.getUserId();
        LOGGER.debug("policy {} for userId {}", policy, userId);

        var request = OpaPermissionRequest.builder()
                .policy(policy)
                .input(input)
                .build();

        var response = opaPermissionClient.check(request);

        LOGGER.debug("policy {} result is {} for userId {}", policy, response.getResult(), userId);
        return response;
    }
}
