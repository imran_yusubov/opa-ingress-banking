package az.ingress.cb.own.payment.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OwnPaymentRepository extends JpaRepository<OwnPaymentEntity, Long> {

}
