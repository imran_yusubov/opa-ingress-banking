package az.ingress.cb.own.payment.client;

import az.ingress.cb.own.payment.model.permission.OpaPermissionRequest;
import az.ingress.cb.own.payment.model.permission.OpaResultResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "opa-permission", url = "${ms.opa-permission.url}")
public interface OpaPermissionClient {

    @PostMapping("/v1/internal/opa-permission/check")
    OpaResultResponse check(@RequestBody OpaPermissionRequest request);

}
