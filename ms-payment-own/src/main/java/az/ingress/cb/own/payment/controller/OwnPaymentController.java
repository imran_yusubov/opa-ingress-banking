package az.ingress.cb.own.payment.controller;

import az.ingress.cb.own.payment.model.HeaderKeys;
import az.ingress.cb.own.payment.model.view.OwnPaymentView;
import az.ingress.cb.own.payment.service.OwnPaymentService;
import az.ingress.common.exception.GlobalErrorAttributes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "${ms.root.url}")
@Api("OWN Accounts Payment Controller")
@Import(GlobalErrorAttributes.class)
public class OwnPaymentController {

    private final OwnPaymentService ownPaymentService;

    public OwnPaymentController(OwnPaymentService ownPaymentService) {
        this.ownPaymentService = ownPaymentService;
    }

    @ApiOperation("Create new OWN payment")
    @PostMapping("/create")
    public ResponseEntity<OwnPaymentView> createPayment(
            @RequestBody OwnPaymentView request,
            @RequestHeader(name = HeaderKeys.HEADER_USER_ID) Long userId,
            @RequestHeader(name = HeaderKeys.HEADER_CUSTOMER_ID) String customerId) {

        OwnPaymentView response = ownPaymentService.createPayment(
                request,
                customerId,
                userId);

        return ResponseEntity.ok(response);
    }

    @ApiOperation("Get one OWN payment")
    @GetMapping("/{paymentId}")
    public ResponseEntity<OwnPaymentView> getPayment(
            @PathVariable Long paymentId,
            @RequestHeader(name = HeaderKeys.HEADER_USER_ID) Long userId,
            @RequestHeader(name = HeaderKeys.HEADER_CUSTOMER_ID) String customerId) {

        OwnPaymentView response = ownPaymentService.getPaymentById(paymentId, customerId, userId);

        return ResponseEntity.ok(response);
    }

}
