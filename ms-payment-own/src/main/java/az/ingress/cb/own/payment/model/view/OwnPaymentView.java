package az.ingress.cb.own.payment.model.view;

import az.ingress.cb.own.payment.model.Amount;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Own accounts payment model")
public class OwnPaymentView {

    @ApiModelProperty("Payment identifier")
    private String id;

    @ApiModelProperty("User id information")
    private Long userId;

    @ApiModelProperty("Debit account")
    private String debitAccount;

    @ApiModelProperty("Credit account")
    private String creditAccount;

    @ApiModelProperty("Transfer amount")
    private Amount amount;

    @ApiModelProperty("Created at")
    private String createdAt;

    @ApiModelProperty("Description")
    private String description;

    @ApiModelProperty("Payment status")
    private String status;

    @ApiModelProperty("Reject reason")
    private String rejectReason;

}
