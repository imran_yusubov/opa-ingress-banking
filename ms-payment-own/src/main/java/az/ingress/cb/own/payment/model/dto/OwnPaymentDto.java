package az.ingress.cb.own.payment.model.dto;

import az.ingress.cb.own.payment.model.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OwnPaymentDto {
    private Long id;
    private String customerId;
    private Long userId;
    private String debitAccount;
    private String creditAccount;
    private BigDecimal transferAmount;
    private String currencyCode;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String description;
    private PaymentStatus status;
    private String rejectReason;
}
